% Version
% --------
% Companion Code Version: 1.0
% 
% 
% License
% --------
% This companion Code for "Cloud K-SVD: A Collaborative Dictionary Learning Algorithm for Big, Distributed Data,"
% IEEE Trans. Signal Process., vol. 64, no. 1, pp. 173�188, 2016, by H. Raja and W. Bajwa
% is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
% 
% Detailed legalese as well as a human-readable summary of this license is available at https://creativecommons.org/licenses/by-nc-sa/4.0/
% 
% 
% Citation
% ---------
% Any part of this code used in your work should be cited as follows:
% 
% H. Raja and W.U. Bajwa, "Cloud K-SVD: A Collaborative Dictionary Learning Algorithm for Big, Distributed Data,"
% IEEE Trans. Signal Process., vol. 64, no. 1, pp. 173�188, 2016, Companion Code, ver. 1.0.
% 
% 
% Reporting of issues
% --------------------
% Any issues in this code should be reported to W.U. Bajwa and H. Raja. However, this companion code is being provided on an "As IS" basis to support the ideals of 
% reproducible research. As such, no guarantees are being made that the reported issues will be eventually fixed.

% Script Details
%---------------
%For fixed value of consensus iterations this script varies number of power
%method iterations and uses functions implementing cloud K-SVD and centralized
%K-SVD to compute following errors:

% 1. It computes representation error of data and results show that the
%representation error are similar for both centralized K-SVD and the cloud
%K-SVD.

% 2. This script also generates errors in dictionaries learned using 
%cloud K-SVD in comparison to dictionaries learned using centralized K-SVD.

%CODE OUTLINE:
%We first generate synthetic data and then distribute data
%across sites in such a way that each site contains subset of subspaces
%spanned by whole data. Finally, representation errors for centralized K-SVD
%and cloud K-SVD are plotted.
%% Initializations
%clear all
clc
addpath('Code\')
sparsityConstraint = 3;
signalDimensions = 20;
TotalSamples = 3e4;
TestSamples = 1e3;
TotalAtoms = 50;
AtomsLocallyAtNodes = 40;
TotalSimulationRuns = 100;
DictionaryLearningIterations = 40;
powerIterationsVector = 2:5;
consensusIterations = 10;
Nodes  = 100;
% Initializing vector for power method
d_init = randn(signalDimensions, 1);
d_init = d_init/norm(d_init);
% Initializing representation errors
TotalRepresentationErrorKSVD = zeros(TotalSimulationRuns, DictionaryLearningIterations+1);
TotalRepresentationErrorCloudKSVD = zeros(TotalSimulationRuns, DictionaryLearningIterations+1, length(powerIterationsVector));
% Initializing Dictionary Errors
TotalDictionaryErrorCloudKSVD = zeros(TotalSimulationRuns, DictionaryLearningIterations, length(powerIterationsVector));
% Initializing final eigenvector error
FinalEigenvectorErrorCloudKSVD = zeros(TotalSimulationRuns, length(powerIterationsVector));
for simulationRuns = 1:TotalSimulationRuns
    %% Generating learning data
    rng(simulationRuns*100, 'twister');
    D = normcols(randn(signalDimensions, TotalAtoms));
    Gamma = zeros(TotalAtoms, TotalSamples);
    AtomsAtNode = zeros(AtomsLocallyAtNodes, Nodes);
    for i = 1:Nodes
        AtomsAtNode(:, i) = randperm(TotalAtoms, AtomsLocallyAtNodes)';
    end
    NodeAtom = 0;
    tic
    for i = 1:TotalSamples
        if rem(i - 1, TotalSamples/Nodes) == 0
            NodeAtom = NodeAtom + 1;
            p = nchoosek(AtomsAtNode(:, NodeAtom), sparsityConstraint);
        end
        Gamma(p(randi([1 size(p, 1)], 1, 1), :),i) = randn(sparsityConstraint,1);
    end
    toc
    Y = D*Gamma;
    snr = 20;
    Y = normcols(Y) + 10^(-snr/20)*normcols(randn(signalDimensions, TotalSamples));
    %% Generating Test data
    Gamma_test = zeros(TotalAtoms, TestSamples);
    AtomsAtNode = zeros(AtomsLocallyAtNodes, Nodes);
    for i = 1:Nodes
        AtomsAtNode(:, i) = randperm(TotalAtoms, AtomsLocallyAtNodes)';
    end
    NodeAtom = 0;
    tic
    for i = 1:TestSamples
        if rem(i - 1, TotalSamples/Nodes) == 0
            NodeAtom = NodeAtom + 1;
            p = nchoosek(AtomsAtNode(:, NodeAtom), sparsityConstraint);
        end
        Gamma_test(p(randi([1 size(p, 1)], 1, 1), :),i) = randn(sparsityConstraint,1);
    end
    toc
    Y_test = D*Gamma_test;
    snr = 20;
    Y_test = normcols(Y_test) + 10^(-snr/20)*normcols(randn(signalDimensions, TestSamples));
    %% Dictionary learning starts here
    %  Initializing dictionary
    D_Initialize = randn(signalDimensions, TotalAtoms);
    D_Initialize = normcols(D_Initialize);
    %% CENTRALIZED K-SVD
    disp('Starting centralized K-SVD')
    [Dksvd, err] = CentralizedKSVD(Y, TotalAtoms, D_Initialize, sparsityConstraint, DictionaryLearningIterations, Y_test);
    TotalRepresentationErrorKSVD(simulationRuns, :) = err;
    %% Initialization for distributed settings
    %  DISTRIBUTING DATA TO NODES
    networkGraph = GenerateGraph(Nodes);
    Y_j = reshape(Y, size(Y, 1), floor(size(Y, 2)/Nodes), Nodes);
    %% Dictionary learning using cloud K-SVD
    disp('Starting cloud K-SVD\n');
    for m = 1:length(powerIterationsVector)
        powerIterations = powerIterationsVector(m);
        D_Initialize_Distributed = repmat(D_Initialize, [1 1 Nodes]);
        [D_CloudKSVD, errDistributedKSVD, errorEigenVector] = CloudKSVD(Y_j, TotalAtoms, D_Initialize_Distributed,...
            sparsityConstraint, Nodes, networkGraph, DictionaryLearningIterations, powerIterations, consensusIterations, Y_test, d_init.');
        TotalRepresentationErrorCloudKSVD(simulationRuns, :, m) = mean(errDistributedKSVD, 1);
        % Compute Error in dictionary
        tic
        disp('Computing error in dictionaries')
        for iter_dic=1:DictionaryLearningIterations
            errTemp = 0;
            for n=1:Nodes
                for k=1:TotalAtoms
                    errTemp = errTemp + norm(Dksvd(:, k, iter_dic)*Dksvd(:, k, iter_dic)' - D_CloudKSVD(:, k, iter_dic, n)*D_CloudKSVD(:, k, iter_dic, n)', 2);
                end
            end
            TotalDictionaryErrorCloudKSVD(simulationRuns, iter_dic, m) = errTemp/(TotalAtoms*Nodes);
        end
        toc
        FinalEigenvectorErrorCloudKSVD(simulationRuns, m) = mean(mean(errorEigenVector(:, end, :)));
    end
    %% Saving Progress so far...
    % Saving representation errors
    save ResultData\VaryingPowerIterations\RepresentationErrorKSVD TotalRepresentationErrorKSVD
    save ResultData\VaryingPowerIterations\RepresentationErrorCloudKSVD TotalRepresentationErrorCloudKSVD
    % Saving eigenvector error
    save ResultData\VaryingPowerIterations\EigenvectorErrorCloudKSVD FinalEigenvectorErrorCloudKSVD
    % Saving Dictionary error
    save ResultData\VaryingPowerIterations\DictionaryErrorCloudKSVD TotalDictionaryErrorCloudKSVD
    fprintf('Monte-Carlo simulations completed: %d\n', simulationRuns)
end