function [atom, errorEigenvector] = distributedPowerMethod(multipliedMatrices, Nodes, networkGraph, powerIterations, consensusIterations, unew)
%% Input list
% multipliedMatrices - 3D array containing matrices E_R E_R^T from each site
% N - Number of nodes/sites in network\
% networkGraph - Doubly stochastic matrix corresponding to the graph
% represnting the interconnections among nodes/sites
%powerIterations - Fixed number of power method iterations that we intend
%to perform
%consensusIterations - Fixed number of iterations for distributed
%summation
%% Output
% atom - updated dictionary atom
%% Initialization
if nargin<6
    unew = randn(1, size(multipliedMatrices, 1));%randomly initializing the eigenvector for power method
    unew = unew/norm(unew);
end
unew = repmat(unew, Nodes, 1);%same random intialization at each site/node
errorEigenvector = zeros(powerIterations, Nodes);
%% POWER METHOD
% Centralized eigenvector computation
[centralizedEigenvector, ~] = eigs(sum(multipliedMatrices, 3), 1);
% distributed power method starts here
for m = 1:powerIterations
    localVectorPowerMethod = zeros(Nodes, size(multipliedMatrices, 1));
    for nodes=1:Nodes
        localVectorPowerMethod(nodes,:) = multipliedMatrices(:,:,nodes) * unew(nodes, :)';
    end
    %CONSENSUS LOOP
    summationMatrix = localVectorPowerMethod;
    for k = 1:consensusIterations
        summationMatrix = networkGraph * summationMatrix;
    end
    for nodes=1:Nodes
       unew(nodes, :) = summationMatrix(nodes, :)/ norm(summationMatrix(nodes, :));
       errorEigenvector(m, nodes) = norm(unew(nodes, :).'*unew(nodes, :) - centralizedEigenvector*centralizedEigenvector.', 2);
    end
end
atom = unew;