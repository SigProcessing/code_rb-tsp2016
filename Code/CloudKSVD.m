function [D, representationError, eigenvectorError] = CloudKSVD(Y_j_input, TotalAtoms, D_init,...
    sparsityConstraint, Nodes, networkGraph, maximumIterations, powerIterations, consensusIterations, Y_test, d_init)
mainIterations = 1;
%Initializing D
D = zeros(size(D_init, 1), size(D_init, 2), maximumIterations+1, Nodes);
D(:, :, 1, :) = D_init;
% Initializing error matrices
representationError = zeros(Nodes, maximumIterations+1);
eigenvectorError = zeros(maximumIterations, powerIterations, Nodes);
if iscell(Y_j_input)==0
    Y_j = cell(Nodes, 1);
    for n=1:Nodes
        Y_j{n} = Y_j_input(:, :, n);
    end
else
    Y_j = Y_j_input;
end
gamma = cell(Nodes, 1);%initializing gamma matrix for each node
while mainIterations<=maximumIterations
    for n = 1:Nodes
        D(:, :, mainIterations, n) = normcols(D(:, :, mainIterations, n));
        D(:, :, mainIterations+1, n) = D(:, :, mainIterations, n);
        gamma{n} = omp(D(:, :, mainIterations+1, n)'*Y_j{n}, D(:, :, mainIterations+1, n)'*D(:, :, mainIterations+1, n), sparsityConstraint);
        if nargin>9
            if ~isempty(Y_test)
                gamma_test = omp(D(:, :, mainIterations+1, n)'*Y_test, D(:, :, mainIterations+1, n)'*D(:, :, mainIterations+1, n), sparsityConstraint);
                representationError(n, mainIterations) = sqrt(sum(sum(Y_test - D(:, :, mainIterations+1, n) * gamma_test).^2)/numel(Y_test));
            end
        end
    end
    p = 1:TotalAtoms;% Sequence of atoms to be updated
    for k = 1:TotalAtoms
        multipliedMatrices = zeros(size(Y_j{1}, 1), size(Y_j{1}, 1), Nodes); %Since each error matrix has same number of rows
        sigma_temp = cell(1, Nodes);
        ER_temp = cell(1, Nodes);
        for n=1:Nodes
            sigma = find(gamma{n}(p(k), :) ~=0);
            sigma_temp{1, n} = sigma;
            if ~isempty(sigma)
                ER = Y_j{n}(:, sigma) - D(:, :, mainIterations+1, n)*gamma{n}(:, sigma) + D(:, p(k), mainIterations+1, n)*gamma{n}(p(k), sigma);
                ER_temp{1, n} = ER;
                multipliedMatrices(:,:, n) = ER * ER.';
            end
        end
        if max(max(max(abs(multipliedMatrices))))>0
            if nargin<11
                [djR, eigenvectorError_temp] = distributedPowerMethod(multipliedMatrices, Nodes, networkGraph, powerIterations, consensusIterations);
            else
                [djR, eigenvectorError_temp] = distributedPowerMethod(multipliedMatrices, Nodes, networkGraph, powerIterations, consensusIterations, d_init);
            end
            eigenvectorError(mainIterations, :, :) = eigenvectorError_temp;
            for n=1:Nodes
                D(:, p(k), mainIterations+1, n) = djR(n, :);
                if ~isempty(sigma_temp{1, n})
                    updatedGamma = ER_temp{1, n}.' * djR(n, :)';
                    gamma{n}(p(k), sigma_temp{1, n}) = updatedGamma;
                end
            end
        end
    end
    mainIterations = mainIterations + 1;
end
if nargin>9
    for n = 1:Nodes
        gamma_test = omp(D(:, :, maximumIterations+1, n)'*Y_test, D(:, :, maximumIterations+1, n)'*D(:, :, maximumIterations+1, n), sparsityConstraint);
        representationError(n, maximumIterations+1) = sqrt(sum(sum(Y_test - D(:, :, maximumIterations+1, n) * gamma_test).^2)/numel(Y_test));
    end
end
end