function [D, err] = CentralizedKSVD(Y, TotalAtoms, D_init, sparsityConstraint, maximumIterations, Y_test)
%This function implements K-SVD algorithm for centrally available data
%INPUT:
%Y -- data samples
%TotalAtoms -- number of atoms/columns in dictionary
%D_init -- initial value of dictionary
%sparsityConstraint -- sparsity constraint for sparse coding problem
%maximumIterations -- iterations of K-SVD algorithm
%Y_test -- test data to compute representation error
%OUTPUT:
%D -- dictionary learned using K-SVD
%err -- array containing representation error for data after each
%dictionary learning iteration
mainIterations = 1;
%Initializing D
D = zeros([size(D_init), maximumIterations+1]);
D(:, :, 1) = D_init;
err = ones(1, maximumIterations+1);
while mainIterations<=maximumIterations
    D(:, :, mainIterations) = normcols(D(:, :, mainIterations));
    D(:, :, mainIterations+1) = D(:, :, mainIterations);
    gamma = omp(D(:, :, mainIterations+1)'*Y, D(:, :, mainIterations+1)'*D(:, :, mainIterations+1), sparsityConstraint);
    if nargin>5
        gamma_test = omp(D(:, :, mainIterations+1)'*Y_test, D(:, :, mainIterations+1)'*D(:, :, mainIterations+1), sparsityConstraint);
        err(mainIterations) = sqrt(sum(sum(Y_test - D(:, :, mainIterations+1)* gamma_test).^2)/numel(Y_test));
    end
    p = 1:TotalAtoms;
    for k = 1:TotalAtoms
        sigma = find(gamma(p(k), :)~=0);
        if ~isempty(sigma)
            ER = Y(:, sigma) - D(:, :, mainIterations+1)*gamma(:, sigma) + D(:, p(k), mainIterations+1)*gamma(p(k), sigma);
            if max(max(abs(ER)))>0
                [u, s, v] = svds(ER, 1);
                D(:, p(k), mainIterations+1) = u;
                updatedGamma = s*v;
                gamma(p(k), sigma) = updatedGamma;
            end
        end
    end
    mainIterations = mainIterations + 1;
end
if nargin>5
    gamma_test = omp(D(:, :, maximumIterations+1)'*Y_test, D(:, :, maximumIterations+1)'*D(:, :, maximumIterations+1), sparsityConstraint);
    err(maximumIterations + 1) = sqrt(sum(sum(Y_test - D(:, :, maximumIterations+1)* gamma_test).^2)/numel(Y_test));
end
end