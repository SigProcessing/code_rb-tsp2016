function  [svmStruct, Best_C, Best_O ]  = svm_classifier_linear( data_t, label_t, i, j )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
class_1 = find( label_t == i);
class_2 = find ( label_t == j );
data_1 = data_t(:,class_1);  %%This is y = 1.
data_2 = data_t(:,class_2); %%We are going to label these as y = -1.
data_a = [data_1 , data_2];
data_a = data_a';
label_data_1 = ones(length(class_1)+length(class_2),1)*(-1);
label_data_1 (1:length(class_1)) = 1;
perm = randperm(length(label_data_1));
data_a = data_a(perm,:);
label_data_1 = label_data_1 (perm);
%%%5-fold Cross validation for choosing C (slack variables) and sigma (in rbf
%%%kernel)
[x y] = size(data_a);
len = floor(x/5);      % data used for validation
C = [0.01 0.03 0.05 0.07 0.1];
%O = [1 2 3];
O = 1; %only linear classifier
ave_error = zeros(length(C),length(O));
for  l = 1:length(C)
    for k = 1:length(O)
        if (i==3 && j==4)
            if (l>2 && k == 1)
                ave_error(l,k)=1000;
                break
            end
        end
        if (i==3 && j==5)
            if (l>2 && k == 1)
                ave_error(l,k)=1000;
                break
            end
        end
        if (i==1 && j==3)
            if l>2
            ave_error(l,k) = 1000;
            break
            end
        end
        Error = zeros(1,5);
        for p = 1:5
            Xtest = data_a((p-1)*len+1 : p*len,:);
            ytest = label_data_1((p-1)*len+1 : p*len);
            Xtrain = [data_a(1:(p-1)*len,:); data_a(p*len+1:end,:)];
            ytrain = [label_data_1(1:(p-1)*len); label_data_1( p*len+1:end)];
            %%%svm
            svmStruct = svmtrain(Xtrain,ytrain,'BoxConstraint',C(l),'kernel_function','linear');
            cs = svmclassify(svmStruct, Xtest); 
            Error(p) = sum(ytest ~= cs)/length(ytest); % mis-classification rate
        end
        ave_error(l,k) = (sum(Error))/5;
    end
end
[minA,ind] = min(ave_error(:));
[a1,a2] = ind2sub(size(ave_error),ind);
Best_C = C(a1);
Best_O = O(a2);
svmStruct = svmtrain(data_a,label_data_1,'BoxConstraint',Best_C,'kernel_function','polynomial','polyorder',Best_O);
end