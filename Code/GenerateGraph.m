function [networkGraph] = GenerateGraph(N, varargin)
if nargin==1
    p=0.5;
else
    p = varargin{1};
end

A = rand(N,N);
A = triu(A,1);
A = A + A';
G = @(A, p) A < p;
networkGraph = double(G(A, p));
n=1;

%% Generating a complete graph
while n<=N
    [a,b] = bfs(networkGraph, 1, n);
    if min(b(n))<0
        A = rand(N,N);
        A = triu(A,1);
        A = A + A';
        networkGraph = double(G(A, p));
        n=1;
    end
    n = n + 1;
end

%% Designing weight matrix for graph G using method from "Fast linear 
%  iterations for distributed averaging" By Lin Xiao and Stephen Boyd
for k=1:N
    networkGraph(k,k)=0;
end
temp = networkGraph;
for k=1:N
    for j=1:N
        networkGraph(k, j) = 1/max(sum(temp(k, :)), sum(temp(j, :)))*temp(k, j);
    end
end
for k=1:N
    networkGraph(k,k)= 1-sum(networkGraph(k, :));
end
end