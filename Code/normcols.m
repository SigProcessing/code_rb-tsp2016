% normcols Takes a matrix as an input and return the same matix 
% after normalizing the columns
function [D_Normalized] = normcols(D)
for k = 1:size(D, 2)
    D(:,k) = D(:,k)/norm(D(:,k));
end
D_Normalized = D;
end