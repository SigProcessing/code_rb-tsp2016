% Version
% --------
% Companion Code Version: 1.0
% 
% 
% License
% --------
% This companion Code for "“Dictionary learning based nonlinear classifier training from distributed data," (GlobalSIP 2014)
% by Z. Shakeri, H. Raja, and W. Bajwa is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
% 
% Detailed legalese as well as a human-readable summary of this license is available at https://creativecommons.org/licenses/by-nc-sa/4.0/
% 
% 
% Citation
% ---------
% Any part of this code used in your work should be cited as follows:
% 
% H. Raja and W.U. Bajwa, "Cloud K-SVD: A Collaborative Dictionary Learning Algorithm for Big, Distributed Data,"
% IEEE Trans. Signal Process., vol. 64, no. 1, pp. 173–188, 2016, Companion Code, ver. 1.0.
% 
% 
% Reporting of issues
% --------------------
% Any issues in this code should be reported to W.U. Bajwa and H. Raja. However, this companion code is being provided on an "As IS" basis to support the ideals of 
% reproducible research. As such, no guarantees are being made that the reported issues will be eventually fixed.


clc
clear all
close all

addpath('ResultData/GlobalSIP2014')

load('Error_Centralized_DKSVD')
load('Error_Distributed_DKSVD')
load('Error_Local_DKSVD')
load('Error_Representative_Cloud_KSVD')

er_c = mean(ErrorCentralizedDKSVD);
er_d = reshape(mean(ErrorDistributedDKSVD, 1), 5, 10);
h_er_d = max(er_d, [], 2);
l_er_d = min(er_d, [], 2);

er_l = reshape(mean(ErrorLocalDKSVD, 1), 5, 10);
h_er_l = max(er_l, [], 2);
l_er_l = min(er_l, [], 2);

er_k = reshape(mean(ErrorCloudKSVD, 1), 5, 10);
h_er_k = max(er_k, [], 2);
l_er_k = min(er_k, [], 2);

plot(1:5, er_c, 's-', 'markers', 10, 'LineWidth', 2)
hold on
errorbar(1:5, mean(er_d'), l_er_d'-mean(er_d'), h_er_d'-mean(er_d'), '*r-', 'markers', 10, 'LineWidth', 2)
hold on
errorbar(1:5, mean(er_l'), l_er_l'- mean(er_l'), h_er_l'- mean(er_l'), '^k-', 'markers', 10, 'LineWidth', 2)
hold on
errorbar(1:5, mean(er_k'), l_er_k'- mean(er_k'), h_er_k'- mean(er_k'), 'dm-', 'markers', 10, 'LineWidth', 2)

%% Plotting SVM
load('Centralized_Linear_SVM_Error');
plot(mean(ErrorCentralizedSVM), '-oc', 'markers', 10, 'LineWidth', 2)

xlabel('Label', 'FontSize', 12, 'FontName', 'Time New Romans')
set(gca, 'XTickLabel', [' ';'0';' ';'1';' ';'2';' ';'3';' ';'4';' '])
ylabel('Classification Error', 'FontSize', 12, 'FontName', 'Time New Romans')
lh = legend('Centralized D-KSVD', 'Cloud D-KSVD','Local D-KSVD', 'Cloud K-SVD','Centralized SVM','FontSize', 10, 'FontName', 'Time New Romans','location','northwest')
M = findobj(lh,'type','line');
set(M, 'linewidth', 2)
set(gca, 'fontsize', 13)
print -dpng -r300 ResultFigures/GlobalSIP2014/Classification_MNIST_Class_balance_r2.png
print -depsc ResultFigures/GlobalSIP2014/Classification_MNIST_Class_balance_r2.eps

%% Plotting for imbalanced data distribution across nodes
load('Error_Centralized_KSVD_Imbalance')
load('Error_Distributed_DKSVD_Imbalance')
load('Error_Local_DKSVD_Imbalance')

er_c = mean(ErrorCentralizedKSVD);

er_d = reshape(mean(ErrorDistributedDKSVD, 1), 5, 10);
h_er_d = max(er_d, [], 2);
l_er_d = min(er_d, [], 2);

er_l = reshape(mean(ErrorLocalDKSVD, 1), 5, 10);
h_er_l = max(er_l, [], 2);
l_er_l = min(er_l, [], 2);

figure
plot(1:5, er_c, 's-', 'markers', 10, 'LineWidth', 2)
hold on
errorbar(1:5, mean(er_d'), l_er_d'-mean(er_d'), h_er_d'-mean(er_d'), '*r-', 'markers', 10, 'LineWidth', 2)
hold on
errorbar(1:5, mean(er_l'), l_er_l'- mean(er_l'), h_er_l'- mean(er_l'), '^k-', 'markers', 10, 'LineWidth', 2)
%% Plotting SVM
load('Centralized_Linear_SVM_Error_Imbalance');
plot(mean(ErrorCentralizedSVM), '-oc', 'markers', 10, 'LineWidth', 2)
xlabel('Label', 'FontSize', 12, 'FontName', 'Time New Romans')
set(gca, 'XTickLabel', [' ';'0';' ';'1';' ';'2';' ';'3';' ';'4';' '])
ylabel('Classification Error', 'FontSize', 12, 'FontName', 'Time New Romans')
lh = legend('Centralized D-KSVD', 'Cloud D-KSVD','Local D-KSVD', 'Centralized SVM', 'FontSize', 14, 'FontName', 'Time New Romans','location','northwest')
M = findobj(lh, 'type', 'line');
set(M, 'linewidth', 2)
set(gca, 'fontsize', 13)
print -dpng -r300 ResultFigures/GlobalSIP2014/Classification_MNIST_Class_imbalance.png
print -depsc ResultFigures/GlobalSIP2014/Classification_MNIST_Class_imbalance.eps