% Version
% --------
% Companion Code Version: 1.0
% 
% 
% License
% --------
% This companion Code for "Cloud K-SVD: A Collaborative Dictionary Learning Algorithm for Big, Distributed Data,"
% IEEE Trans. Signal Process., vol. 64, no. 1, pp. 173�188, 2016, by H. Raja and W. Bajwa
% is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
% 
% Detailed legalese as well as a human-readable summary of this license is available at https://creativecommons.org/licenses/by-nc-sa/4.0/
% 
% 
% Citation
% ---------
% Any part of this code used in your work should be cited as follows:
% 
% H. Raja and W.U. Bajwa, "Cloud K-SVD: A Collaborative Dictionary Learning Algorithm for Big, Distributed Data,"
% IEEE Trans. Signal Process., vol. 64, no. 1, pp. 173�188, 2016, Companion Code, ver. 1.0.
% 
% 
% Reporting of issues
% --------------------
% Any issues in this code should be reported to W.U. Bajwa and H. Raja. However, this companion code is being provided on an "As IS" basis to support the ideals of 
% reproducible research. As such, no guarantees are being made that the reported issues will be eventually fixed.


clc
clear all
close all

addpath('ResultData/VaryingPowerIterations')
addpath('ResultData/VaryingConsensusIterations')
addpath('ResultData/OnlineKSVD')
%% PLOTTING Figure 2(a)
% loading eigenvector errors
load ErrorEigenvectorCloudKSVD
figure;
semilogy(errorEigenvectorCloudKSVD_Plot(1, :), 'r-*', 'markers', 10, 'LineWidth', 3)
hold on
semilogy(errorEigenvectorCloudKSVD_Plot(2, :), '--', 'markers', 10, 'LineWidth', 3)
semilogy(errorEigenvectorCloudKSVD_Plot(3, :), 'k^-', 'markers', 10, 'LineWidth', 3)
semilogy(errorEigenvectorCloudKSVD_Plot(4, :), 'gs-', 'markers', 10, 'LineWidth', 3)
xlabel('Power method iteration (t_p)', 'FontSize', 20, 'FontName', 'Arial')
ylabel('$\|q_c q_c^T - \widehat{q}_i^{(t)}\widehat{q}_i^{(t)^T}\|_2$','Interpreter', 'Latex', 'FontSize', 20, 'FontName', 'Arial')
h = legend('2 consensus iter.', '3 consensus iter.', '5 consensus iter.',...
    '8 consensus iter.')
set(h,  'Location', 'Best')
set(h, 'FontSize',20, 'FontName', 'Arial');
set(gca,'FontSize',20, 'FontName', 'Arial')
print -dpng -r300 ResultFigures/TSP2015/EigenvectorErrorCloudKSVDVaryingConsensusIterations.png
print(gcf, '-depsc', 'ResultFigures/TSP2015/EigenvectorErrorCloudKSVDVaryingConsensusIterations.eps');
print(gcf, '-dpdf', 'ResultFigures/TSP2015/EigenvectorErrorCloudKSVDVaryingConsensusIterations.pdf');


%% PLOTTING Figures 2(b) and 2(c)
%% Loading data files
load RepresentationErrorKSVD_OneConsensus
load RepresentationErrorCloudKSVD_OneConsensus
% Loading Dictionary error
load DictionaryErrorCloudKSVD_OneConsensus
load errLocalKSVD_new
DictionaryErrorMoreThanOneConsensus  = load('DictionaryErrorCloudKSVD');

%% Computing vetors to plot
% Representation Errors
simulationRuns = size(TotalRepresentationErrorKSVD, 1);
RepresentationErrorKSVD_Plot = sum(TotalRepresentationErrorKSVD)/simulationRuns;
RepresentationErrorCloudKSVD_Plot = sum(TotalRepresentationErrorCloudKSVD, 1)/simulationRuns;
% Dictionary error
DictionaryErrorCloudKSVD_Plot = sum(TotalDictionaryErrorCloudKSVD, 1)/simulationRuns;
DictionaryErrorMoreThanOneConsnsus_Plot = mean(DictionaryErrorMoreThanOneConsensus.TotalDictionaryErrorCloudKSVD, 1);
%% Plotting
% Plotting representation error for different values of power method
% iterations
figure;
hold on
box on
plot(RepresentationErrorKSVD_Plot, 's-', 'Markers', 10, 'LineWidth', 3)
plot(RepresentationErrorCloudKSVD_Plot(:, :, 1), 'r*', 'Markers', 10, 'LineWidth', 3)
plot(RepresentationErrorCloudKSVD_Plot(:, :, 2), 'ko', 'Markers', 10, 'LineWidth', 3)
plot(RepresentationErrorCloudKSVD_Plot(:, :, 3), '^', 'Markers', 10, 'LineWidth', 3)
plot(RepresentationErrorCloudKSVD_Plot(:, :, 4), 'g+', 'Markers', 10, 'LineWidth', 3)
plot(errLocalKSVD_Plot, 'ks', 'Markers', 10, 'LineWidth', 3)
xlabel('Dictionary learning iteration (t)', 'FontSize', 20, 'FontName', 'Arial')
ylabel('Average Representation Error', 'FontSize', 20, 'FontName', 'Arial')
h = legend('K-SVD', '2 Power Iter.', '3 Power Iter.', '4 Power Iter.', '5 Power Iter.', 'Local K-SVD');
set(h,  'Location', 'NorthEast')
set(h, 'FontSize', 20, 'FontName', 'Arial');
set(gca,'FontSize', 20, 'FontName', 'Arial')
print -dpng -r300 ResultFigures/TSP2015/RepresentationErrorOneConsensus.png
print(gcf, 'ResultFigures/TSP2015/RepresentationErrorOneConsensus.eps', '-depsc')
print(gcf, '-dpdf', 'ResultFigures/TSP2015/RepresentationErrorOneConsensus.pdf');

%% Plotting error in dictionaries
figure;
[ax,h1,h2] = plotyy(1:size(DictionaryErrorMoreThanOneConsnsus_Plot, 2), DictionaryErrorCloudKSVD_Plot(1, :, 1),...
     1:size(DictionaryErrorMoreThanOneConsnsus_Plot, 2), DictionaryErrorMoreThanOneConsnsus_Plot(1, :, 1), 'semilogy');
axis(ax(1), [0 40 1e-4 1])
axis(ax(2), [0 40 1e-4 1])
set(h1, 'Color', 'b', 'LineWidth', 3, 'LineStyle', '-', 'Marker', '*', 'MarkerSize', 10)
set(h2, 'Color', 'r', 'LineWidth', 3, 'LineStyle', '-', 'Marker', '*', 'MarkerSize', 10)
set(ax(1), 'YTickLabelMode', 'auto', 'YTickMode', 'auto')
set(ax(2), 'YTickLabelMode', 'auto', 'YTickMode', 'auto')
hold(ax(1))
plot(ax(1), 1:size(DictionaryErrorMoreThanOneConsnsus_Plot, 2), DictionaryErrorCloudKSVD_Plot(1, :, 2)...
    , 'o', 'Markers', 10, 'LineWidth', 3)
hold(ax(2))
plot(ax(2), 1:size(DictionaryErrorMoreThanOneConsnsus_Plot, 2), DictionaryErrorMoreThanOneConsnsus_Plot(1, :, 2)...
    , 'ro-', 'Markers', 10, 'LineWidth', 3)

 plot(ax(1), 1:size(DictionaryErrorMoreThanOneConsnsus_Plot, 2), DictionaryErrorCloudKSVD_Plot(1, :, 3)...
 , '^', 'Markers', 10, 'LineWidth', 3)
 plot(ax(2), 1:size(DictionaryErrorMoreThanOneConsnsus_Plot, 2), DictionaryErrorMoreThanOneConsnsus_Plot(1, :, 3)...
     , 'r^-', 'Markers', 10, 'LineWidth', 3)
 
 plot(ax(1), 1:size(DictionaryErrorMoreThanOneConsnsus_Plot, 2), DictionaryErrorCloudKSVD_Plot(1, :, 4)...
     , 's', 'Markers', 10, 'LineWidth', 3)
plot(ax(2), 1:size(DictionaryErrorMoreThanOneConsnsus_Plot, 2), DictionaryErrorMoreThanOneConsnsus_Plot(1, :, 4)...
    , 'rs-', 'Markers', 10, 'LineWidth', 3)

xlabel(ax(1), 'Dictionary learning iteration (t)', 'FontSize',...
    20, 'FontName', 'Arial', 'Color', 'black', 'HorizontalAlignment', 'center')
ylabel(ax(1), '$E_{avg}^{(t)}$', 'Interpreter', 'Latex', 'FontSize', 20, 'FontName', 'Arial', 'Color', 'black')
set(ax(1), 'FontSize', 20, 'FontName', 'Arial', 'YColor', 'k')
set(ax(2), 'FontSize', 20, 'FontName', 'Arial', 'YTick', [], 'YTickLabel', [], 'Visible', 'Off')
h = legend(ax(1), 'T_p=2, T_c=1', 'T_p=3, T_c=1', 'T_p=4, T_c=1', 'T_p=5, T_c=1');
set(h, 'FontSize', 20, 'FontName', 'Arial');
set(h, 'Location', 'SouthEast');
h2 = legend(ax(2), 'T_p=2, T_c=10', 'T_p=3, T_c=10', 'T_p=4, T_c=10', 'T_p=5, T_c=10');
set(h2,  'Location', 'SouthWest', 'color', 'w')
set(h2, 'FontSize', 20, 'FontName', 'Arial');
print -dpng -r300 ResultFigures/TSP2015/DictionaryErrorOneConsensus.png
print('ResultFigures/TSP2015/DictionaryErrorOneConsensus.eps', '-depsc')
print(gcf, '-dpdf', 'ResultFigures/TSP2015/DictionaryErrorOneConsensus.pdf', '-r300');

%% PLOTTING Figure 2(d)
load('RepresentationErrorOnlineKSVD_Forgetting.mat');
load('DictionaryErrorOnlineVsCentralized');
DictionaryLearningIterations = 60;
OlineSteps = 6;
figure;
box on
hold on
[ax,h1,h2] = plotyy(1:61, errKSVD_Plot(1:61),...
     1:size(ErrorOnlineVsCentralized_Plot, 2), ErrorOnlineVsCentralized_Plot)
axis(ax(1), [0 365 0 0.14])
axis(ax(2), [0 6 0 0.14])
%set(ax(2), 'XAxisLocation', 'top')
%set(ax(1), 'XAxisLocation', 'bottom')
set(h1, 'Color', 'b', 'LineWidth', 3, 'LineStyle', '-')
set(h2, 'Color', 'r', 'LineWidth', 3, 'LineStyle', '-', 'Marker', '*', 'MarkerSize', 10)
set(ax(1), 'YTickLabelMode', 'auto', 'YTickMode', 'auto')
set(ax(2), 'YTickLabelMode', 'auto', 'YTickMode', 'auto')
set(ax(1), 'YColor', [0 0 0])
set(ax(2), 'YColor', [0 0 0]) 
plot(ax(1), 62:122, errKSVD_Plot(62:122), 'r-', 'LineWidth', 3)
plot(ax(1), 122:182, errKSVD_Plot(122:182), 'g-', 'LineWidth', 3)
plot(ax(1), 183:243, errKSVD_Plot(183:243), 'c-', 'LineWidth', 3)
plot(ax(1), 244:304, errKSVD_Plot(244:304), 'm-', 'LineWidth', 3)
plot(ax(1), 305:365, errKSVD_Plot(305:365), 'y-', 'LineWidth', 3)
plot(ax(1), 61:61:365, errKSVD_Plot(61:61:365), 'ko', 'Markers', 10, 'LineWidth', 4)
set(ax(2),'XTickLabel',[])
set(ax(2),'XTick',[])
set(ax(2),'YTick',[])
xlabel(ax(1), 'Dictionary learning iteration (t)', 'FontSize', 20, 'FontName', 'Arial')
ylabel(ax(1), 'Error', 'FontSize', 20, 'FontName', 'Arial', 'Color', 'k')
%ylabel(ax(2), 'Error in dictionaries', 'FontSize', 20, 'FontName', 'Arial', 'Color', 'k')
h = legend(ax(1), 'Representation error, Batch 1', 'Representation error, Batch 2',...
    'Representation error, Batch 3', 'Representation error, Batch 4',...
    'Representation error, Batch 5', 'Representation error, Batch 6', 'Arrival of new data')
%set(h,  'Location', 'Best')
set(h, 'position', [0.6 0.68 0.03 0.03])
set(h, 'FontSize', 20, 'FontName', 'Arial');

h2 = legend(ax(2), 'Avg. dev. per dictionary atom')
set(h2, 'position', [0.595 0.4 0.03 0.03])
set(h2, 'FontSize', 20, 'FontName', 'Arial');
set(gca,'FontSize', 20, 'FontName', 'Arial')
set(ax(2),'FontSize', 20, 'FontName', 'Arial')
print('ResultFigures/TSP2015/OnlineKSVD.eps', '-depsc')
print -dpng -r300 ResultFigures/TSP2015/OnlineKSVD.png
print(gcf, '-dpdf', 'ResultFigures/TSP2015/OnlineKSVD.pdf');