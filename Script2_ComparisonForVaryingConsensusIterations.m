% Version
% --------
% Companion Code Version: 1.0
% 
% 
% License
% --------
% This companion Code for "Cloud K-SVD: A Collaborative Dictionary Learning Algorithm for Big, Distributed Data,"
% IEEE Trans. Signal Process., vol. 64, no. 1, pp. 173�188, 2016, by H. Raja and W. Bajwa
% is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
% 
% Detailed legalese as well as a human-readable summary of this license is available at https://creativecommons.org/licenses/by-nc-sa/4.0/
% 
% 
% Citation
% ---------
% Any part of this code used in your work should be cited as follows:
% 
% H. Raja and W.U. Bajwa, "Cloud K-SVD: A Collaborative Dictionary Learning Algorithm for Big, Distributed Data,"
% IEEE Trans. Signal Process., vol. 64, no. 1, pp. 173�188, 2016, Companion Code, ver. 1.0.
% 
% 
% Reporting of issues
% --------------------
% Any issues in this code should be reported to W.U. Bajwa and H. Raja. However, this companion code is being provided on an "As IS" basis to support the ideals of 
% reproducible research. As such, no guarantees are being made that the reported issues will be eventually fixed.

% Script Details
%---------------
%For fixed value of power method this script varies number of consensus
%iterations and uses functions implementing cloud K-SVD and centralized
%K-SVD to compute following errors:

% 1. It computes representation error of data and results show that the
%representation error decreases for both the algorithms. It also computes
%representation error using local K-SVD, where only local data is used to
%learn dictionary.

% 2. This script also computes errors in distributed dictionaries learned
% using cloud K-SVD as compared to the dictionaries learned using
% centralized K-SVD.

%CODE OUTLINE
%------------
%We first generate synthetic data and then distribute data
%across sites in such a way that each site contains subset of subspaces
%spanned by whole data. In these settings we also learned dictionaries
%using K-SVD for only locally available data for each site. Finally,
%representation errors for centralized K-SVD, cloud K-SVD, and local K-SVD
%are plotted.
%% Initializations
%clear all
clc
addpath('.\Code')
sparsityConstraint = 3;
signalDimensions = 20;
TotalSamples = 3e4;
TestSamples = 1e3;
TotalAtoms = 50;
AtomsLocallyAtNodes = 40;
TotalSimulationRuns = 100;
DictionaryLearningIterations = 30;
powerIterations = 10;
consensusIterationsVector = [3 4 5 8 10];
Nodes  = 100;
% Initializing vector for power method
d_init = randn(signalDimensions, 1);
d_init = d_init/norm(d_init);
% Initializing representation errors
TotalErrorKSVD = zeros(TotalSimulationRuns, DictionaryLearningIterations+1);
TotalErrorCloudKSVD = zeros(TotalSimulationRuns, DictionaryLearningIterations+1, length(consensusIterationsVector));
TotalErrorLocalKSVD = zeros(TotalSimulationRuns, DictionaryLearningIterations+1, Nodes);
% Initializing errors in eigenvectors
MeanErrorDistributedEigenvectorCloudKSVD = zeros(length(consensusIterationsVector), powerIterations, TotalSimulationRuns);
for simulationRuns = 1:TotalSimulationRuns
    %% Generating learning data
    rng(simulationRuns*100, 'twister');
    D = normcols(randn(signalDimensions, TotalAtoms));
    Gamma = zeros(TotalAtoms, TotalSamples);
    AtomsAtNode = zeros(AtomsLocallyAtNodes, Nodes);
    for i = 1:Nodes
        AtomsAtNode(:, i) = randperm(TotalAtoms, AtomsLocallyAtNodes)';
    end
    NodeAtom = 0;
    tic
    for i = 1:TotalSamples
        if rem(i - 1, TotalSamples/Nodes) == 0
            NodeAtom = NodeAtom + 1;
            p = nchoosek(AtomsAtNode(:, NodeAtom), sparsityConstraint);
        end
        Gamma(p(randi([1 size(p, 1)], 1, 1), :),i) = randn(sparsityConstraint,1);
    end
    toc
    Y = D*Gamma;
    snr = 20;
    Y = normcols(Y) + 10^(-snr/20)*normcols(randn(signalDimensions, TotalSamples));
    %% Generating Test data
    Gamma_test = zeros(TotalAtoms, TestSamples);
    AtomsAtNode = zeros(AtomsLocallyAtNodes, Nodes);
    for i = 1:Nodes
        AtomsAtNode(:, i) = randperm(TotalAtoms, AtomsLocallyAtNodes)';
    end
    NodeAtom = 0;
    tic
    for i = 1:TestSamples
        if rem(i - 1, TotalSamples/Nodes) == 0
            NodeAtom = NodeAtom + 1;
            p = nchoosek(AtomsAtNode(:, NodeAtom), sparsityConstraint);
        end
        Gamma_test(p(randi([1 size(p, 1)], 1, 1), :),i) = randn(sparsityConstraint,1);
    end
    toc
    Y_test = D*Gamma_test;
    snr = 20;
    Y_test = normcols(Y_test) + 10^(-snr/20)*normcols(randn(signalDimensions, TestSamples));
    %% Dictionary learning starts here
    %  Initializing dictionary
    D_Initialize = randn(signalDimensions, TotalAtoms);
    D_Initialize = normcols(D_Initialize);
    %% CENTRALIZED K-SVD
    disp('Starting centralized K-SVD')
    [Dksvd, err] = CentralizedKSVD(Y, TotalAtoms, D_Initialize, sparsityConstraint, DictionaryLearningIterations, Y_test);
    TotalErrorKSVD(simulationRuns, :) = err;
    %% Initialization for distributed settings
    %  DISTRIBUTING DATA TO NODES
    networkGraph = GenerateGraph(Nodes);
    columnsPerSubMatrix = size(Y, 2)/Nodes;
    Y_j = zeros(length(Y(:,1)), columnsPerSubMatrix, Nodes);
    for k = 0:Nodes-1
        if k~=Nodes-1
            Y_j(:,:,k + 1) = Y(:, ((k*columnsPerSubMatrix)+1):((k+1)*columnsPerSubMatrix));
        else
            Y_j(:,:,Nodes) = zeros(length(Y(:,1)), length(Y(1,:)) - (Nodes-1)*columnsPerSubMatrix);
            Y_j(:,:,Nodes) = Y(:, ((k*columnsPerSubMatrix)+1):end);
        end
    end
    %% Dictionary learning using cloud K-SVD
    disp('Starting cloud K-SVD\n');
    for c = 1:length(consensusIterationsVector)
        consensusIterations = consensusIterationsVector(c);
        D_Initialize_Distributed = repmat(D_Initialize, [1 1 Nodes]);
        [D_KSVD, errDistributedKSVD, errorDistributedEigenvector_1] = CloudKSVD(Y_j, TotalAtoms, D_Initialize_Distributed,...
            sparsityConstraint, Nodes, networkGraph, DictionaryLearningIterations, powerIterations, consensusIterations, Y_test, d_init.');
        MeanErrorDistributedEigenvectorCloudKSVD(c, :, simulationRuns) = mean(mean(errorDistributedEigenvector_1, 3), 1);
        TotalErrorCloudKSVD(simulationRuns, :, c) = mean(errDistributedKSVD, 1);
    end
    %% LOCAL DICTIONARIES
    %  In Local dictionary learning nodes only use locally available
    %  data to learn dictionary using K-SVD
    disp('Starting K-SVD using only locally available data\n')
    for k=1:Nodes
        [Dksvd, err] = CentralizedKSVD(Y_j(:, :, k), TotalAtoms, D_Initialize, sparsityConstraint, DictionaryLearningIterations, Y_test);
        TotalErrorLocalKSVD(simulationRuns, :, k) = err;
    end
    %% Computing average errors uptil current simulation run
    % Representation Errors
    errKSVD_Plot = sum(TotalErrorKSVD)/simulationRuns;
    errDistributedKSVD_Plot = sum(TotalErrorCloudKSVD, 1)/simulationRuns;
    errLocalKSVD_Plot = sum(sum(TotalErrorLocalKSVD, 3), 1)/(simulationRuns*Nodes);
    errBarLocalKSVD = sqrt(var(TotalErrorLocalKSVD))/Nodes;
    % Eigenvector errors
    errorEigenvectorCloudKSVD_Plot = sum(MeanErrorDistributedEigenvectorCloudKSVD, 3)/simulationRuns;
    errorEigenvectorMatrixConsensus_Plot = sum(MeanErrorDistributedEigenvector_M, 3)/simulationRuns;
    % Saving representation errors
    save ResultData/VaryingConsensusIterations/RepresentationErrorKSVD errKSVD_Plot
    save ResultData/VaryingConsensusIterations/RepresentationErrorLocalKSVD errLocalKSVD_Plot
    save ResultData/VaryingConsensusIterations/RepresentationErrorBarLocalKSVD errBarLocalKSVD
    save ResultData/VaryingConsensusIterations/RepresentationErrorCloudKSVD errDistributedKSVD_Plot
    % Saving eigenvector errors
    save ResultData/VaryingConsensusIterations/ErrorEigenvectorCloudKSVD errorEigenvectorCloudKSVD_Plot
    fprintf('Monte-Carlo simulations completed: %d\n', simulationRuns)
end