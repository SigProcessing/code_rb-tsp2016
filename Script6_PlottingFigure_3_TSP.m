% Version
% --------
% Companion Code Version: 1.0
% 
% 
% License
% --------
% This companion Code for "Cloud K-SVD: A Collaborative Dictionary Learning Algorithm for Big, Distributed Data,"
% IEEE Trans. Signal Process., vol. 64, no. 1, pp. 173�188, 2016, by H. Raja and W. Bajwa
% is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
% 
% Detailed legalese as well as a human-readable summary of this license is available at https://creativecommons.org/licenses/by-nc-sa/4.0/
% 
% 
% Citation
% ---------
% Any part of this code used in your work should be cited as follows:
% 
% H. Raja and W.U. Bajwa, "Cloud K-SVD: A Collaborative Dictionary Learning Algorithm for Big, Distributed Data,"
% IEEE Trans. Signal Process., vol. 64, no. 1, pp. 173�188, 2016, Companion Code, ver. 1.0.
% 
% 
% Reporting of issues
% --------------------
% Any issues in this code should be reported to W.U. Bajwa and H. Raja. However, this companion code is being provided on an "As IS" basis to support the ideals of 
% reproducible research. As such, no guarantees are being made that the reported issues will be eventually fixed.


clc
clear all
close all

addpath('ResultData/MNIST')
load('TP_Rate_Centralized_MNIST_RandomNew');
load('TP_Rate_DistributedMNIST_RandomNew_10');
load('TP_Rate_LocalMNIST_RandomNew_10');
plot(mean(TP_Rate_Centralized, 2), 'r*-', 'markers', 10, 'LineWidth', 3)
hold on
plot(1:5, mean(mean(TP_Rate_Distributed, 2), 3), '^-', 'LineWidth', 3, 'markers', 10)
plot(1:5, mean(mean(TP_Rate_Local, 2), 3), 'g-', 'LineWidth', 3, 'markers', 10)

axis([0.5 5.5 0.8 1])
set(gca,'XTickLabel',['0';'3';'5';'8';'9';' '])
xlabel('Class labels (digits)', 'FontSize', 20, 'FontName', 'Arial')
ylabel('Average Detection Rate', 'FontSize', 20, 'FontName', 'Arial')
h = legend('Centralized K-SVD', 'Cloud K-SVD', 'Local K-SVD', 'Location', 'SouthWest');
set(h, 'FontSize', 20, 'FontName', 'Arial')

L = abs(mean(mean(TP_Rate_Distributed, 2), 3) - min(mean(TP_Rate_Distributed, 3), [], 2));
U = abs(mean(mean(TP_Rate_Distributed, 2), 3) - max(mean(TP_Rate_Distributed, 3), [], 2));
errorbar(1:5, mean(mean(TP_Rate_Distributed, 2), 3), L, U, '^-', 'LineWidth', 3, 'markers', 10)

%plot(mean(mean(TP_Rate_Local, 2), 3), 'g.')
L = abs(mean(mean(TP_Rate_Local, 2), 3) - min(mean(TP_Rate_Local, 3), [], 2));
U = abs(mean(mean(TP_Rate_Local, 2), 3) - max(mean(TP_Rate_Local, 3), [], 2));
errorbar(1:5, mean(mean(TP_Rate_Local, 2), 3), L, U, 'g.', 'LineWidth', 3, 'markers', 10)
set(gca,'FontSize', 20, 'FontName', 'Arial')

print -depsc ResultFigures/TSP2015/Classification_MNIST.eps
print -dpng -r300 ResultFigures/TSP2015/Classification_MNIST.png
print -dpdf -r300 ResultFigures/TSP2015/Classification_MNIST.pdf