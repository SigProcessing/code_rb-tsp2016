% Version
% --------
% Companion Code Version: 1.0
% 
% 
% License
% --------
% This companion Code for "“Dictionary learning based nonlinear classifier training from distributed data," (GlobalSIP 2014)
% by Z. Shakeri, H. Raja, and W. Bajwa is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
% 
% Detailed legalese as well as a human-readable summary of this license is available at https://creativecommons.org/licenses/by-nc-sa/4.0/
% 
% 
% Citation
% ---------
% Any part of this code used in your work should be cited as follows:
% 
% H. Raja and W.U. Bajwa, "Cloud K-SVD: A Collaborative Dictionary Learning Algorithm for Big, Distributed Data,"
% IEEE Trans. Signal Process., vol. 64, no. 1, pp. 173–188, 2016, Companion Code, ver. 1.0.
% 
% 
% Reporting of issues
% --------------------
% Any issues in this code should be reported to W.U. Bajwa and H. Raja. However, this companion code is being provided on an "As IS" basis to support the ideals of 
% reproducible research. As such, no guarantees are being made that the reported issues will be eventually fixed.
%
% Script Details
%---------------
% This script parses MNIST dataset and perform classification over first
% five digits using following methods:
% 1. Linear SVM
% 2. Learning representative dictionary for each class in distributed
%    settings using cloud K-SVD and using residue based method proposed by ...
%    to perform classification
% 3. For centralized settings learning discriminative dictionary using
%    method proposed by...
% 4. Distributed version of D-K-SVD algorithm, using cloud K-SVD as an
%    engine to perform distributed dictionary learning
% 5. Distributed version of D-K-SVD algorithm, where only locally available
%    date is used to learn the dictionary that is used to classification
%    purposes

clc
clear all
close all
addpath('.\Code')
D_Distributed = zeros(256, 500, 10, 5);
NumberOfClasses = 5;
MaximumInitializationIterations = 10;
MaximumIterations = 50;
Alpha = 0.69;
%% Initializations and pre-processing
rng(sum(100*clock));
load('mnist_all.mat')
SizeTrainingData = [length(train0), length(train1),length(train2),length(train3),length(train4)];
SizeTestData = [length(test0) length(test1) length(test2) length(test3) length(test4)];
TrainingData = [train0(1:SizeTrainingData(1),:); train1(1:SizeTrainingData(2),:); train2(1:SizeTrainingData(3),:); train3(1:SizeTrainingData(4),:); train4(1:SizeTrainingData(5),:)];
TrainingLabels = [ones(SizeTrainingData(1),1); 2*ones(SizeTrainingData(2),1); 3*ones(SizeTrainingData(3),1); 4*ones(SizeTrainingData(4),1); 5*ones(SizeTrainingData(5),1)];
TestData = [test0; test1; test2; test3; test4];
TestLabels = [ones(length(test0),1); 2*ones(length(test1),1); 3*ones(length(test2),1); 4*ones(length(test3),1); 5*ones(length(test4),1)];

TrainingData = double(TrainingData)/255;
TrainingLabels = double(TrainingLabels);
TestData = double(TestData)/255;
TestLabels = double(TestLabels);

CompleteData = [TrainingData; TestData];
CompleteLabels = [TrainingLabels; TestLabels];

%Downsampling the data
DataDimensions = size(CompleteData, 2);
DownSampledDimensions = 256;
DownSampledData = zeros(DownSampledDimensions, size(CompleteData, 1)); %downsampling 784 dim data to 256 dim
for m=1:size(CompleteData, 1)
    DownSampledData(:, m) = imresize(CompleteData(m, :), DownSampledDimensions/DataDimensions);
end
CompleteData = DownSampledData;
len = floor(length(CompleteData)/5);
ll= zeros(NumberOfClasses,1);
ll(1) = 1;
labels1 = [];
for i = 1:NumberOfClasses
    labels1 = [labels1 repmat(ll,[1, SizeTrainingData(i)])];
    ll =circshift(ll,1);
end
l2= zeros(NumberOfClasses,1);
l2(1) = 1;
labels2 = [];
for i = 1:NumberOfClasses
    labels2 = [labels2 repmat(l2,[1, SizeTestData(i)])];
    l2 = circshift(l2,1);
end
labelss = [labels1 , labels2];
permuteData = randperm(length(CompleteData));
data_c = CompleteData;
label_c = CompleteLabels;
CompleteData = CompleteData(:,permuteData);
CompleteLabels = CompleteLabels(permuteData);
labelss = labelss(:,permuteData);
% Initializing error matrices
ErrorCloudKSVD =  zeros(CrossValidationCount, NumberOfClasses, Nodes);
ErrorCentralizedSVM = zeros(CrossValidationCount, NumberOfClasses);
ErrorCentralizedDKSVD = zeros(CrossValidationCount, NumberOfClasses);
ErrorDistributedDKSVD = zeros(CrossValidationCount, NumberOfClasses, Nodes);
ErrorLocalDKSVD = zeros(CrossValidationCount, NumberOfClasses, Nodes);
for CrossValidation = 1:5
    TestData = CompleteData(:,(CrossValidation-1)*len+1 : CrossValidation*len);
    TestLabels = CompleteLabels((CrossValidation-1)*len+1 : CrossValidation*len);
    
    data_test_r = data_c(:,(CrossValidation-1)*len+1 : CrossValidation*len);
    label_test_r = label_c((CrossValidation-1)*len+1 : CrossValidation*len);
    
    TrainingData = [CompleteData(:,1:(CrossValidation-1)*len), CompleteData(:,CrossValidation*len+1:end)];
    TrainingLabels = [CompleteLabels(1:(CrossValidation-1)*len); CompleteLabels( CrossValidation*len+1:end)];
    labels = [labelss(:,1:(CrossValidation-1)*len), labelss(:,CrossValidation*len+1:end)];
    
    data_t_r = [data_c(:,1:(CrossValidation-1)*len), data_c(:,CrossValidation*len+1:end)];
    label_t_r = [label_c(1:(CrossValidation-1)*len); label_c( CrossValidation*len+1:end)];
    
    DataDimensions = DownSampledDimensions;
    TotalAtoms = 500; %Total number of atoms in dictionary
    % data = normcols(data);
    Nodes = 10;
    %     permuteData = randperm(floor(size(labels, 2)/Nodes)*Nodes);
    TrainingData = TrainingData(:, 1:floor(size(labels, 2)/Nodes)*Nodes);
    labels = labels(:, 1:floor(size(labels, 2)/Nodes)*Nodes);
    TrainingLabels = TrainingLabels(1:floor(size(labels, 2)/Nodes)*Nodes);
    
    data_t_r = data_t_r(:, 1:floor(size(labels, 2)/Nodes)*Nodes);
    label_t_r = label_t_r(1:floor(size(labels, 2)/Nodes)*Nodes);
    
    %Distributing data across nodes/sites
    data_t_distributed = reshape(TrainingData(:, 1:floor(size(TrainingData, 2)/Nodes)*Nodes), size(TrainingData, 1), floor(size(TrainingData, 2)/Nodes), Nodes);
    labels_distributed = reshape(labels(:, 1:floor(size(labels, 2)/Nodes)*Nodes), size(labels, 1), floor(size(labels, 2)/Nodes), Nodes);
    L = 10; %Sparsity Level
    %% Centralized Linear SVM
    res_c = zeros(length(TestLabels), NumberOfClasses);
    for i = 1:NumberOfClasses-1
        for k = i+1:NumberOfClasses
            [svmStruct, Best_C, Best_O]  = svm_classifier_linear( TrainingData, TrainingLabels, i, k );
            result = svmclassify(svmStruct, TestData');
            res_c( result == 1,i) = res_c( result == 1,i) + 1 ;
            res_c( result == -1,k) = res_c( result == -1,k) + 1 ;
        end
    end
    [x, y] = max(res_c, [], 2);
    sub = y - double(TestLabels)';
    sub(sub  ~= 0 ) = 1 ;
    %%%Errors for each class
    for i = 1:NumberOfClasses
        MissClassifications = TestLabels(TestLabels == i)'- y(TestLabels == i);
        MissClassifications(MissClassifications  ~= 0 ) = 1 ;
        ErrorCentralizedSVM(CrossValidation, i) = (sum(MissClassifications))/length(find(TestLabels == i));
    end
    save ResultData/GlobalSIP2014/Centralized_Linear_SVM_Error ErrorCentralizedSVM
    
    %% Initializations Distributed Setting
    %Initializing Dictionary: Each node initializes dictionary with
    %randomly selected samples as atoms of dictionary.
    D_init_d = randn(DataDimensions, TotalAtoms, Nodes);
    for n=1:Nodes
        if TotalAtoms>size(data_t_distributed, 2)
            D_init_1 = [ data_t_distributed(:, : , n) randn(DataDimensions, TotalAtoms - size(data_t_distributed, 2))];
        else
            D_init_1 = data_t_distributed(:, 1:TotalAtoms, n);
        end
        D_init_d(:, :, n) = normcols(D_init_1);
    end
    
    % Generating network graph. For details on graph generation please read
    % details given at the start of GenerateGraph function.
    networkGraph = GenerateGraph(Nodes);
    
    %% Representative Dictionary
    % When representative dictionary is used for classification we learn
    % separate dictionary for each class using cloud K-SVD.
    disp('Training Representative Dictionary')
    D_Initialize = zeros(DataDimensions, TotalAtoms/NumberOfClasses, Nodes);
    AtomsPerClass = 100;
    AtomCount = AtomsPerClass*ones(1, NumberOfClasses);
    length_class = zeros(1, NumberOfClasses);
    for k=1:NumberOfClasses
        d_shape = TrainingData(:, TrainingLabels == k);
        length_class(k) = length(find(TrainingLabels==k));
        d_shape = (d_shape(:,1:floor(0.1*length(d_shape))*10));
        Y_j = reshape(d_shape, DataDimensions, floor(length_class(k)/Nodes), Nodes); %Y_j is the matrix of samples at site/node j
        data_t_distributed_r = Y_j;
        for n=1:Nodes
            D_init_1 = Y_j(:, 1:TotalAtoms/5, n);
            D_Initialize(:, :, n) = normcols(D_init_1);
        end
        [Dm, errDistributedKSVD] = CloudKSVD....
            (data_t_distributed_r, TotalAtoms/5, D_Initialize, L, Nodes, networkGraph, MaximumIterations+MaximumInitializationIterations, 15, 15);
        D_Distributed(:, sum(AtomCount(1:k-1))+1:sum(AtomCount(1:k)), :, CrossValidation) = Dm(:, :, :);
    end
    %% Classification Distributed
    ClassificationResults = zeros(1, size(TestData, 2));
    datasetMatrix = TestData;
    residue = zeros(1, size(TestData, 2), NumberOfClasses);
    for nodes=1:Nodes
        gamma = omp(D_Distributed(:,:, nodes, CrossValidation)'*datasetMatrix,...
            D_Distributed(:,:, nodes, CrossValidation)'*D_Distributed(:,:, nodes, CrossValidation), L);
        for m=1:NumberOfClasses
            Dm = zeros(DataDimensions, TotalAtoms/NumberOfClasses);
            if m>1
                Dm(:, :) = D_Distributed(:, sum(AtomCount(1:m-1))+1:sum(AtomCount(1:m)), nodes, CrossValidation);
                gamma_m = gamma(sum(AtomCount(1:m-1))+1:sum(AtomCount(1:m)), :);
            else
                Dm(:, :) = D_Distributed(:, 1:AtomCount(m), nodes, CrossValidation);
                gamma_m = gamma(sum(AtomCount(1:m-1))+1:sum(AtomCount(1:m)), :);
            end
            residue(:, :, m) = (sum(datasetMatrix - Dm * gamma_m).^2).^2;
        end
        [value, ClassificationLabels] = min(sqrt(residue), [], 3);
        ClassificationResults = ClassificationLabels;
        for m = 1:NumberOfClasses
            MissClassifications = TestLabels(TestLabels == m)'- ClassificationResults(TestLabels == m);
            MissClassifications(MissClassifications  ~= 0 ) = 1 ;
            ErrorCloudKSVD(CrossValidation, m, nodes) = (sum(MissClassifications))/length(find(TestLabels == m));
        end
    end
    save ResultData/GlobalSIP2014/Error_Representative_Cloud_KSVD ErrorCloudKSVD
    %% Centralized D-K_SVD
    disp('Training Discriminative Dictionary using D-K-SVD')
    %Initializing the dictionary and W
    disp('Dictionary initialization using K-SVD');
    D_init_c = TrainingData(:,1:TotalAtoms);
    D_init_c = normcols(D_init_c);
    [DiscriminativeDictionaryCentralized, ~] = CentralizedKSVD(TrainingData, TotalAtoms, D_init_c, L , MaximumInitializationIterations);
    %W
    alpha1_c = omp(DiscriminativeDictionaryCentralized'*TrainingData,...
        DiscriminativeDictionaryCentralized'*DiscriminativeDictionaryCentralized, L);
    W_1 = (alpha1_c*alpha1_c'+eye(TotalAtoms))\(alpha1_c*labels');
    DiscriminativeDictionaryCentralized = [DiscriminativeDictionaryCentralized; Alpha*W_1'];
    DiscriminativeDictionaryCentralized = normcols(DiscriminativeDictionaryCentralized);
    Dic_init_phase2 = DiscriminativeDictionaryCentralized;
    %PHASE 2 - Performing iterations to find a better dictionary
    disp('Starting to  train the discriminative dictionary');
    DataWithLables = [TrainingData;labels*Alpha];
    [DiscriminativeDictionaryCentralized, output] =  CentralizedKSVD(DataWithLables, TotalAtoms, Dic_init_phase2 , L , MaximumIterations);
    %Renormalizing dic and W
    Dic_c_new = normcols(DiscriminativeDictionaryCentralized(1:end-NumberOfClasses,:));
    W = DiscriminativeDictionaryCentralized(end-NumberOfClasses+1:end,:)./repmat(sqrt(sum(DiscriminativeDictionaryCentralized(1:end-NumberOfClasses,:).^2)),[NumberOfClasses 1]);
    W = W/(Alpha);
    %Classification
    alpha_c = omp(Dic_c_new'*TestData, Dic_c_new'*Dic_c_new, L);
    l = W *alpha_c;
    [maxA, ~] =max(l);
    for i = 1:NumberOfClasses
        MissClassifications = TestLabels(TestLabels == i)'- b(TestLabels == i);
        MissClassifications(MissClassifications  ~= 0 ) = 1 ;
        ErrorCentralizedDKSVD(CrossValidation,i) = (sum(MissClassifications))/length(find(TestLabels == i));
    end
    save ResultData/GlobalSIP2014/Error_Centralized_DKSVD ErrorCentralizedDKSVD
    %% Distributed D-K_SVD
    disp('Training Distributed Discriminative Dictionary')
    %Initializing the dictionary and W
    disp('Dictionary initialization using K-SVD');
    %D_init same as above
    [DiscriminativeDictionaryDistributed, ~] = CloudKSVD(data_t_distributed, TotalAtoms, D_init_d, L, Nodes, networkGraph, MaximumInitializationIterations,15,15);
    %W
    Dic_d_new = zeros(size(DiscriminativeDictionaryDistributed, 1)+NumberOfClasses, size(DiscriminativeDictionaryDistributed, 2), size(DiscriminativeDictionaryDistributed, 3));
    for n=1:Nodes
        alpha1_c = omp(DiscriminativeDictionaryDistributed(:, :, n)'*data_t_distributed(:, :, n),...
            DiscriminativeDictionaryDistributed(:, :, n)'*DiscriminativeDictionaryDistributed(:, :, n), L);
        W = (alpha1_c*alpha1_c'+eye(TotalAtoms))\(alpha1_c*labels_distributed(:, :, n)');
        Dic_d_new(:, :, n) = [DiscriminativeDictionaryDistributed(:, :, n); Alpha*W'];
        Dic_d_new(:, :, n) = normcols(Dic_d_new(:, :, n));
    end
    %Performing iterations to find a better dictionary
    disp('Starting to  train the discriminative dictionary');
    data_ttt_distributed = zeros(size(data_t_distributed, 1)+NumberOfClasses, size(data_t_distributed, 2), size(data_t_distributed, 3));
    for n=1:Nodes
        data_ttt_distributed(:, :, n) = [data_t_distributed(:, :, n);labels_distributed(:, :, n)*Alpha];
    end
    [DiscriminativeDictionaryDistributed, err] = CloudKSVD(data_ttt_distributed, TotalAtoms, Dic_d_new, L, Nodes, networkGraph, MaximumIterations,15,15);
    W_d = [];
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    for n=1:Nodes
        %Renormalizing dic and W
        Dic_d_new = normcols(DiscriminativeDictionaryDistributed(1:end-NumberOfClasses,:, n));
        W = DiscriminativeDictionaryDistributed(end-NumberOfClasses+1:end,:, n)./repmat(sqrt(sum(DiscriminativeDictionaryDistributed(1:end-NumberOfClasses,:, n).^2)),[NumberOfClasses 1]);
        W = W/(Alpha);
        %Classification
        alpha_c = omp(Dic_d_new'*TestData, Dic_d_new'*Dic_d_new, L);
        l = W *alpha_c;
        [maxA ~] = max(l);
        sub = b - double(TestLabels)';
        sub(sub  ~= 0 ) = 1 ;
        for i = 1:NumberOfClasses
            MissClassifications = TestLabels(TestLabels == i)'- b(TestLabels == i);
            MissClassifications(MissClassifications  ~= 0 ) = 1 ;
            ErrorDistributedDKSVD(CrossValidation,i,n) = (sum(MissClassifications))/length(find(TestLabels == i));
        end  
    end
    save ResultData/GlobalSIP2014/Error_Distributed_DKSVD ErrorDistributedDKSVD
    %% Local D-K_SVD
    disp('Training Local Discriminative Dictionary')
    W_l =[];
    for n=1:Nodes
        %Initializing the dictionary and W
        disp('Dictionary initialization using K-SVD');
        D_init_l = data_t_distributed(:, 1:TotalAtoms, n);
        D_init_l = normcols(D_init_l);
        [DiscriminativeDictionaryLocal, ~] = CentralizedKSVD(data_t_distributed(:, :, n), TotalAtoms, D_init_l, L , MaximumInitializationIterations);
        alpha1_c = omp(DiscriminativeDictionaryLocal'*data_t_distributed(:, :, n), DiscriminativeDictionaryLocal'*DiscriminativeDictionaryLocal, L);
        W = (alpha1_c*alpha1_c'+eye(TotalAtoms))\(alpha1_c*labels_distributed(:, :, n)');
        DiscriminativeDictionaryLocal = [DiscriminativeDictionaryLocal; Alpha*W'];
        Dic_c_1 = normcols(DiscriminativeDictionaryLocal);
        %Performing iterations to find a better dictionary
        disp('Starting to  train the discriminative dictionary');
        DataWithLables = [data_t_distributed(:, :, n);labels_distributed(:, :, n)*Alpha];
        [DiscriminativeDictionaryCentralized, output] = CentralizedKSVD(DataWithLables, TotalAtoms, Dic_c_1, L , MaximumIterations);
        %Renormalizing dic and W
        Dic_l_new = normcols(DiscriminativeDictionaryLocal(1:end-NumberOfClasses,:));
        W = DiscriminativeDictionaryLocal(end-NumberOfClasses+1:end,:)./repmat(sqrt(sum(DiscriminativeDictionaryLocal(1:end-NumberOfClasses,:).^2)),[NumberOfClasses 1]);
        W = W/(Alpha);
        %Classification
        alpha_c = omp(Dic_l_new'*TestData, Dic_l_new'*Dic_l_new, L);
        l = W *alpha_c;
        [maxA ~] =max(l);        
        for i = 1:NumberOfClasses
            MissClassifications = TestLabels(TestLabels == i)'- b(TestLabels == i);
            MissClassifications(MissClassifications  ~= 0 ) = 1 ;
            ErrorLocalDKSVD(CrossValidation,i,n) = (sum(MissClassifications))/length(find(TestLabels == i));
        end
    end
    save ResultData/GlobalSIP2014/Error_Local_DKSVD ErrorLocalDKSVD
end