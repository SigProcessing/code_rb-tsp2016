% Version
% --------
% Companion Code Version: 1.0
% 
% 
% License
% --------
% This companion Code for "Cloud K-SVD: A Collaborative Dictionary Learning Algorithm for Big, Distributed Data,"
% IEEE Trans. Signal Process., vol. 64, no. 1, pp. 173�188, 2016, by H. Raja and W. Bajwa
% is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
% 
% Detailed legalese as well as a human-readable summary of this license is available at https://creativecommons.org/licenses/by-nc-sa/4.0/
% 
% 
% Citation
% ---------
% Any part of this code used in your work should be cited as follows:
% 
% H. Raja and W.U. Bajwa, "Cloud K-SVD: A Collaborative Dictionary Learning Algorithm for Big, Distributed Data,"
% IEEE Trans. Signal Process., vol. 64, no. 1, pp. 173�188, 2016, Companion Code, ver. 1.0.
% 
% 
% Reporting of issues
% --------------------
% Any issues in this code should be reported to W.U. Bajwa and H. Raja. However, this companion code is being provided on an "As IS" basis to support the ideals of 
% reproducible research. As such, no guarantees are being made that the reported issues will be eventually fixed.

% Script Details
%---------------
%
%Images of digits 0, 3, 5, 8, and 9 from MNIST dataset are used for testing
%cloud K-SVD for classification task. 6000 images from each class are used
%in total. 5-fold cross validation is performed and in each fold 5000
%samples for each digit are used training samples. Classification results
%using cenralized K-SVD, cloud K-SVD, and local K-SVD are obtained.
%% Initializations
%clear all
clc
addpath('Code\')
datasetStruct = load('mnist_all');
Nodes = 10;
dataset0 = datasetStruct.train0;
dataset3 = datasetStruct.train3;
dataset5 = datasetStruct.train5;
dataset8 = datasetStruct.train8;
dataset9 = datasetStruct.train9;
groundTruth0 = datasetStruct.test0;
groundTruth3 = datasetStruct.test3;
groundTruth5 = datasetStruct.test5;
groundTruth8 = datasetStruct.test8;
groundTruth9 = datasetStruct.test9;
%Taking total of 6000 samples per class and using 5000 for training and
%1000 for test purposes
data0 = [dataset0; groundTruth0];
data0 = data0(1:6000, :);
data3 = [dataset3; groundTruth3];
data3 = data3(1:6000, :);
data5 = [dataset5; groundTruth5];
data5 = data5(1:6000, :);
data8 = [dataset8; groundTruth8];
data8 = data8(1:6000, :);
data9 = [dataset9; groundTruth9];
data9 = data9(1:6000, :);
CompleteData = [data0; data3; data5; data8; data9];
%Downsampling data
CompleteDataDownSampled = zeros(256, size(CompleteData, 1)); %downsampling 784 dim data to 256 dim
for m=1:size(CompleteData, 1)
    CompleteDataDownSampled(:, m) = imresize(CompleteData(m, :), 256/784);
end
TrainingSamples = ones(1, 5)*1000;
 groundTruthLabels = [zeros(1, 1000), 3*ones(1, 1000), 5*ones(1, 1000),...
     8*ones(1, 1000), 9*ones(1, 1000)];
NumberOfClasses = 5;
powerIterations = 15;
consensusIterations = 15;
rng(100); %Initializing random generator state
TotalAtoms = ones(1, NumberOfClasses)*300;
crossValidation = 5;
TP_Rate_Centralized = zeros(NumberOfClasses, crossValidation);
TP_Rate_Distributed = zeros(NumberOfClasses, Nodes, crossValidation);
TP_Rate_Local = zeros(NumberOfClasses, Nodes, crossValidation);
sparsityConstraint = 10;
signalDimensions = 256;
maximumIterations = 30;
D_Centralized = zeros(signalDimensions, sum(TotalAtoms), crossValidation);
D_Distributed = zeros(signalDimensions, sum(TotalAtoms), Nodes, crossValidation);
D_Local = zeros(signalDimensions, sum(TotalAtoms), Nodes, crossValidation);
TrainingData = zeros(256, 5000*NumberOfClasses);
for validation = 1:crossValidation
    indices = randperm(6000);
    TrainingData = [CompleteDataDownSampled(:, indices(1:5000)), CompleteDataDownSampled(:, 6000+indices(1:5000)),...
        CompleteDataDownSampled(:, 2*6000+indices(1:5000)),...
        CompleteDataDownSampled(:, 3*6000+indices(1:5000)), CompleteDataDownSampled(:, 4*6000+indices(1:5000))];
    testData = [CompleteDataDownSampled(:, indices(5001:end)), CompleteDataDownSampled(:, 6000+indices(5001:end)),...
        CompleteDataDownSampled(:, 2*6000+indices(5001:end)),...
        CompleteDataDownSampled(:, 3*6000+indices(5001:end)), CompleteDataDownSampled(:, 4*6000+indices(5001:end))];
    %% CENTRALIZED DICTIONARY LEARNING
    for k=1:NumberOfClasses
        Ym = TrainingData(:, 5000*(k-1)+1:k*5000);
        if TrainingSamples(k)>TotalAtoms(k)
            tic
            %Initializing dictionary
            D_Initialize = randn(signalDimensions, TotalAtoms(k));
            D_Initialize = normcols(D_Initialize);
            D_Initialize_Save(:, (k-1)*TotalAtoms(k)+1:k*TotalAtoms(k), validation) = D_Initialize;
            %save D_Initialize_Save D_Initialize_Save
            %CENTRALIZED METHODS
            [Dm, err] = CentralizedKSVD(Ym, TotalAtoms(k), D_Initialize, sparsityConstraint, maximumIterations);
            if k>1
                D_Centralized(:, sum(TotalAtoms(1:k-1))+1:sum(TotalAtoms(1:k)), validation) = Dm;
            else
                D_Centralized(:, 1:TotalAtoms(k), validation) = Dm;
            end
            toc
        else
            D_Centralized = Ym;
        end
    end
    save ResultData\MNIST\CentralizedDictionaryMNIST_RandomNew D_Centralized
    
    %% CLASSIFICATION
    
    Classified = zeros(1, size(testData, 2));
    datasetMatrix = testData;
    residue = zeros(1, size(testData, 2), NumberOfClasses);
    tic
    gamma = omp(D_Centralized(:,:, validation)'*datasetMatrix, D_Centralized(:,:, validation)'*D_Centralized(:,:, validation), sparsityConstraint);
    for m=1:NumberOfClasses
        Dm = zeros(signalDimensions, TotalAtoms(m));
        if m>1
            Dm(:, :) = D_Centralized(:, sum(TotalAtoms(1:m-1))+1:sum(TotalAtoms(1:m)), validation);
            gamma_m = gamma(sum(TotalAtoms(1:m-1))+1:sum(TotalAtoms(1:m)), :);
        else
            Dm(:, :) = D_Centralized(:, 1:TotalAtoms(m), validation);
            gamma_m = gamma(sum(TotalAtoms(1:m-1))+1:sum(TotalAtoms(1:m)), :);
        end
        residue(:, :, m) = (sum(datasetMatrix - Dm * gamma_m).^2).^2;
    end
    
    [value, Classified_1] = min(sqrt(residue), [], 3);
    Classified(Classified_1==1) = 0;
    Classified(Classified_1==2) = 3;
    Classified(Classified_1==3) = 5;
    Classified(Classified_1==4) = 8;
    Classified(Classified_1==5) = 9;
    labels = [0, 3, 5, 8, 9];
    for m=1:NumberOfClasses
        classIndex = (find(groundTruthLabels==labels(m)));
        TP_Rate_Centralized(m, validation) = length(find(Classified(classIndex) ==  groundTruthLabels(classIndex)))/(length(classIndex));
    end
    save ResultData\MNIST\TP_Rate_Centralized_MNIST_RandomNew TP_Rate_Centralized
    
    %% DISTRIBUTED DICTIONARY LEARNING
    networkGraph = GenerateGraph(Nodes, 0.2);
    initializingIterations = 35;
    for k=1:NumberOfClasses
        Y_j = reshape(TrainingData(:, 5000*(k-1)+1:k*5000), size(TrainingData, 1), 500, Nodes);
        signalDimensions = size(Y_j, 1);
        if TrainingSamples(k)>TotalAtoms(k)
            tic
            %CENTRALIZED METHODS
            D_Initialize = D_Initialize_Save(:, (k-1)*TotalAtoms(k)+1:k*TotalAtoms(k), validation);
            D_Initialize_2 = zeros(signalDimensions, TotalAtoms(k), Nodes);
            for n=1:Nodes
                D_Initialize_2(:, :, n) = D_Initialize;
            end
            [Dm, errDistributedKSVD] = ...
                CloudKSVD(Y_j, TotalAtoms(k), D_Initialize_2, sparsityConstraint, Nodes, networkGraph, maximumIterations, powerIterations, consensusIterations);
            if k>1
                D_Distributed(:, sum(TotalAtoms(1:k-1))+1:sum(TotalAtoms(1:k)), :, validation) = Dm(:, :, :);
            else
                D_Distributed(:, 1:TotalAtoms(k), :, validation) = Dm(:, :, :);
            end
            toc
        else
            Dm = Ym;
        end
        save ResultData\MNIST\DistributedDictionariesConsensus10_RandomNew D_Distributed
    end
    
    %% Classification Distributed
    Classified = zeros(1, size(testData, 2));
    datasetMatrix = testData;
    residue = zeros(1, size(testData, 2), NumberOfClasses);
    tic
    for nodes=1:Nodes
        gamma = omp(D_Distributed(:,:, nodes, validation)'*datasetMatrix, D_Distributed(:,:, nodes, validation)'*D_Distributed(:,:, nodes, validation), sparsityConstraint);
        for m=1:NumberOfClasses
            Dm = zeros(signalDimensions, TotalAtoms(m));
            if m>1
                Dm(:, :) = D_Distributed(:, sum(TotalAtoms(1:m-1))+1:sum(TotalAtoms(1:m)), nodes, validation);
                gamma_m = gamma(sum(TotalAtoms(1:m-1))+1:sum(TotalAtoms(1:m)), :);
            else
                Dm(:, :) = D_Distributed(:, 1:TotalAtoms(m), nodes, validation);
                gamma_m = gamma(sum(TotalAtoms(1:m-1))+1:sum(TotalAtoms(1:m)), :);
            end
            residue(:, :, m) = (sum(datasetMatrix - Dm * gamma_m).^2).^2;
        end
        [value, Classified_1] = min(sqrt(residue), [], 3);
        Classified(Classified_1==1) = 0;
        Classified(Classified_1==2) = 3;
        Classified(Classified_1==3) = 5;
        Classified(Classified_1==4) = 8;
        Classified(Classified_1==5) = 9;
        labels = [0, 3, 5, 8, 9];
        for m=1:NumberOfClasses
            classIndex = (find(groundTruthLabels==labels(m)));
            TP_Rate_Distributed(m, nodes, validation) = length(find(Classified(classIndex) ==  groundTruthLabels(classIndex)))/(length(classIndex));
        end
    end
    save ResultData\MNIST\TP_Rate_DistributedMNIST_RandomNew_10 TP_Rate_Distributed
    %% Local Learning
    for k=1:NumberOfClasses
        Y_j = reshape(TrainingData(:, 5000*(k-1)+1:k*5000), size(TrainingData, 1), 500, Nodes);
        signalDimensions = size(Y_j, 1);
        if TrainingSamples(k)>TotalAtoms(k)
            tic
            %Initializing dictionary
            %CENTRALIZED METHODS
            for nodes = 1:Nodes
                [Dm, err] = CentralizedKSVD(Y_j(:, :, nodes), TotalAtoms(k), D_Initialize, sparsityConstraint, maximumIterations);
                if k>1
                    D_Local(:, sum(TotalAtoms(1:k-1))+1:sum(TotalAtoms(1:k)), nodes, validation) = Dm;
                else
                    D_Local(:, 1:TotalAtoms(k), nodes, validation) = Dm;
                end
            end
            toc
        else
            D_Local = Ym;
        end
        save LocalDictionaries10_RandomNew D_Local
    end
    %% CLASSIFICATION Local
    Classified = zeros(1, size(testData, 2));
    datasetMatrix = testData;
    residue = zeros(1, size(testData, 2), NumberOfClasses);
    tic
    for nodes=1:Nodes
        gamma = omp(D_Local(:,:, nodes, validation)'*datasetMatrix, D_Local(:,:, nodes, validation)'*D_Local(:,:, nodes, validation), sparsityConstraint);
        for m=1:NumberOfClasses
            Dm = zeros(signalDimensions, TotalAtoms(m));
            if m>1
                Dm(:, :) = D_Local(:, sum(TotalAtoms(1:m-1))+1:sum(TotalAtoms(1:m)), nodes, validation);
                gamma_m = gamma(sum(TotalAtoms(1:m-1))+1:sum(TotalAtoms(1:m)), :);
            else
                Dm(:, :) = D_Local(:, 1:TotalAtoms(m), nodes, validation);
                gamma_m = gamma(sum(TotalAtoms(1:m-1))+1:sum(TotalAtoms(1:m)), :);
            end
            residue(:, :, m) = (sum(datasetMatrix - Dm * gamma_m).^2).^2;
        end
        [value, Classified_1] = min(sqrt(residue), [], 3);
        Classified(Classified_1==1) = 0;
        Classified(Classified_1==2) = 3;
        Classified(Classified_1==3) = 5;
        Classified(Classified_1==4) = 8;
        Classified(Classified_1==5) = 9;
        labels = [0, 3, 5, 8, 9];
        for m=1:NumberOfClasses
            classIndex = (find(groundTruthLabels==labels(m)));
            TP_Rate_Local(m, nodes, validation) = length(find(Classified(classIndex) ==  groundTruthLabels(classIndex)))/(length(classIndex))
        end
    end    
    toc
    save ResultData\MNIST\TP_Rate_LocalMNIST_RandomNew_10 TP_Rate_Local
end