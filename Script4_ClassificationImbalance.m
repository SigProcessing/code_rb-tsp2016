% Version
% --------
% Companion Code Version: 1.0
% 
% 
% License
% --------
% This companion Code for "“Dictionary learning based nonlinear classifier training from distributed data," (GlobalSIP 2014)
% by Z. Shakeri, H. Raja, and W. Bajwa is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License.
% 
% Detailed legalese as well as a human-readable summary of this license is available at https://creativecommons.org/licenses/by-nc-sa/4.0/
% 
% 
% Citation
% ---------
% Any part of this code used in your work should be cited as follows:
% 
% H. Raja and W.U. Bajwa, "Cloud K-SVD: A Collaborative Dictionary Learning Algorithm for Big, Distributed Data,"
% IEEE Trans. Signal Process., vol. 64, no. 1, pp. 173–188, 2016, Companion Code, ver. 1.0.
% 
% 
% Reporting of issues
% --------------------
% Any issues in this code should be reported to W.U. Bajwa and H. Raja. However, this companion code is being provided on an "As IS" basis to support the ideals of 
% reproducible research. As such, no guarantees are being made that the reported issues will be eventually fixed.
%
% Script Details
%---------------
% This script parses MNIST dataset and perform classification over first
% five digits using following methods:
% 1. Linear SVM
% 2. Learning representative dictionary for each class in distributed
%    settings using cloud K-SVD and using residue based method proposed by ...
%    to perform classification
% 3. For centralized settings learning discriminative dictionary using
%    method proposed by...
% 4. Distributed version of D-K-SVD algorithm, using cloud K-SVD as an
%    engine to perform distributed dictionary learning
% 5. Distributed version of D-K-SVD algorithm, where only locally available
%    date is used to learn the dictionary that is used to classification
%    purposes

%%%%%%%%%%Difference from ClassificationUnifrom.m%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script is performing the same tasks as are performed by
% ClassificationUniform.m. The only difference is that in case of
% ClassificationUniform.m we had same amount of samples at each site. While in
% this script we divided data unequally i.e., each site contains different
% number of samples.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc
clear all
close all
addpath('Code');
NumberOfClasses = 5;
maxiterations = 10;
maximumIterations = 50;
Nodes = 10;
TotalAtoms = 500;
Alpha = 0.69;
%% Initializations and pre-processing
rng(sum(100*clock));
load('mnist_all.mat')
TrainingDataSize = [length(train0), length(train1),length(train2),length(train3),length(train4)];
TestDataSize = [length(test0) length(test1) length(test2) length(test3) length(test4)];
TrainingData = [train0(1:TrainingDataSize(1),:);train1(1:TrainingDataSize(2),:);train2(1:TrainingDataSize(3),:);train3(1:TrainingDataSize(4),:);train4(1:TrainingDataSize(5),:)];
TrainingLabels = [ones(TrainingDataSize(1),1);2*ones(TrainingDataSize(2),1); 3*ones(TrainingDataSize(3),1); 4*ones(TrainingDataSize(4),1); 5*ones(TrainingDataSize(5),1)];
TestData = [test0; test1; test2; test3; test4];
TestLabels = [ones(length(test0),1); 2*ones(length(test1),1); 3*ones(length(test2),1); 4*ones(length(test3),1); 5*ones(length(test4),1)];
TrainingData = double(TrainingData)/255;
TrainingLabels = double(TrainingLabels);
TestData = double(TestData)/255;
TestLabels = double(TestLabels);
Data = [TrainingData;TestData];
Labels = [TrainingLabels; TestLabels];
%Downsampling the data
DownSampledDimensions = 256;
Data1 = zeros(DownSampledDimensions, size(Data, 1)); %downsampling 784 dim data to 256 dim
for m=1:size(Data, 1)
    Data1(:, m) = imresize(Data(m, :), DownSampledDimensions/784);
end
Data = Data1;
len = floor(length(Data)/5);
ll= zeros(NumberOfClasses,1);
ll(1) = 1;
% Creating matrix H (by the name BinaryLabelMatrix here) as defined in
% “Discriminative K-SVD for dictionary learning in face recognition,” 
% by Q. Zhang and B. Li 
BinaryLabelMatrix1 = [];
for i = 1:NumberOfClasses
    BinaryLabelMatrix1 = [BinaryLabelMatrix1 repmat(ll, [1, TrainingDataSize(i)])];
    ll = circshift(ll, 1);
end
l2 = zeros(NumberOfClasses, 1);
l2(1) = 1;
BinaryLabelMatrix2 = [];
for i = 1:NumberOfClasses
        BinaryLabelMatrix2 = [BinaryLabelMatrix2 repmat(l2, [1, TestDataSize(i)])];
        l2 = circshift(l2,1);
end
BinaryLabelMatrix = [BinaryLabelMatrix1 , BinaryLabelMatrix2];
permuteData = randperm(length(Data));
Data = Data(:,permuteData);
Labels = Labels(permuteData);
BinaryLabelMatrix = BinaryLabelMatrix(:,permuteData);
DataDimensions = DownSampledDimensions;
CrossValidationCount = 5;
% Initializing error matrices
ErrorCentralizedLinearSVM = zeros(CrossValidationCount, NumberOfClasses);
ErrorCentralizedKSVD = zeros(CrossValidationCount, NumberOfClasses);
ErrorDistributedDKSVD = zeros(CrossValidationCount, NumberOfClasses, Nodes);
ErrorLocalDKSVD = zeros(CrossValidationCount, NumberOfClasses, Nodes);
for CrossValidation = 1:CrossValidationCount
    TestData = Data(:,(CrossValidation-1)*len+1 : CrossValidation*len);
    TestLabels = Labels((CrossValidation-1)*len+1 : CrossValidation*len);
    
    TrainingData = [Data(:,1:(CrossValidation-1)*len), Data(:,CrossValidation*len+1:end)];
    TrainingLabels = [Labels(1:(CrossValidation-1)*len); Labels( CrossValidation*len+1:end)];
    labels = [BinaryLabelMatrix(:,1:(CrossValidation-1)*len), BinaryLabelMatrix(:,CrossValidation*len+1:end)];
    
    TrainingData = TrainingData(:, 1:floor(size(labels, 2)/Nodes)*Nodes);
    labels = labels(:, 1:floor(size(labels, 2)/Nodes)*Nodes);
    TrainingLabels = TrainingLabels(1:floor(size(labels, 2)/Nodes)*Nodes);
    
    data_1 = TrainingData(:,1:floor(0.8*length(TrainingData)));
    data_2 = TrainingData(:,floor(0.8*length(TrainingData))+1:end);
    
    labels_1 = labels(:,1:floor(0.8*length(TrainingData)));
    labels_2 = labels(:,floor(0.8*length(TrainingData))+1:end);
    %Distributing data
    data_t_distributed_1 = reshape(data_1(:, 1:floor(size(data_1, 2)*2/Nodes)*Nodes/2), size(data_1, 1), floor(size(data_1, 2)*2/Nodes), Nodes/2);
    labels_distributed_1 = reshape(labels_1(:, 1:floor(size(labels_1, 2)*2/Nodes)*Nodes/2), size(labels_1, 1), floor(size(labels_1, 2)*2/Nodes), Nodes/2);
    
    data_t_distributed_2 = reshape(data_2(:, 1:floor(size(data_2, 2)*2/Nodes)*Nodes/2), size(data_2, 1), floor(size(data_2, 2)*2/Nodes), Nodes/2);
    labels_distributed_2 = reshape(labels_2(:, 1:floor(size(labels_2, 2)*2/Nodes)*Nodes/2), size(labels_2, 1), floor(size(labels_2, 2)*2/Nodes), Nodes/2);
    
    data_t_distributed = {data_t_distributed_1(:,:,1) ; data_t_distributed_1(:,:,2); data_t_distributed_1(:,:,3)  ;data_t_distributed_1(:,:,4);...
        data_t_distributed_1(:,:,5) ;data_t_distributed_2(:,:,1);data_t_distributed_2(:,:,2); data_t_distributed_2(:,:,3);data_t_distributed_2(:,:,4);data_t_distributed_2(:,:,5)};
    labels_distributed = {labels_distributed_1(:,:,1) ; labels_distributed_1(:,:,2); labels_distributed_1(:,:,3)  ;labels_distributed_1(:,:,4);...
        labels_distributed_1(:,:,5) ;labels_distributed_2(:,:,1);labels_distributed_2(:,:,2); labels_distributed_2(:,:,3);labels_distributed_2(:,:,4);labels_distributed_2(:,:,5)};
    %Sparsity Level
    L = 10;    
    %% Centralized Linear SVM
    %%%SVM classifiers
    res_c = zeros(length(TestLabels), NumberOfClasses);
    for i = 1:NumberOfClasses-1
        for k = i+1:NumberOfClasses
            [svmStruct, Best_C, Best_O]  = svm_classifier_linear( TrainingData, TrainingLabels, i, k );
            result = svmclassify(svmStruct, TestData');
            res_c( result == 1, i) = res_c( result == 1, i) + 1 ;
            res_c( result == -1, k) = res_c( result == -1, k) + 1 ;
        end
    end
    [x, y] = max(res_c, [], 2);
    %%%Errors for each class
    for i = 1:NumberOfClasses
        sub = TestLabels(TestLabels == i)'- y(TestLabels == i);
        sub(sub  ~= 0 ) = 1 ;
        ErrorCentralizedLinearSVM(CrossValidation, i) = (sum(sub))/length(find(TestLabels == i));
    end
    save ResultData/GlobalSIP2014/Centralized_Linear_SVM_Error_Imbalance ErrorCentralizedLinearSVM
    %% Centralized D-K_SVD
    disp('Training Centralized Discriminative Dictionary')
    %Initializing the dictionary and W
    disp('Dictionary initialization using K-SVD');
    D_init = TrainingData(:,1:TotalAtoms);
    D_init = normcols(D_init);
    [DictionaryCentralizedDKSVD, ~] = CentralizedKSVD(TrainingData, TotalAtoms, D_init, L , maxiterations);
    alpha1_c = omp(DictionaryCentralizedDKSVD'*TrainingData, DictionaryCentralizedDKSVD'*DictionaryCentralizedDKSVD, L);
    W_1 = (alpha1_c*alpha1_c'+eye(TotalAtoms))\(alpha1_c*labels');
    DictionaryCentralizedDKSVD = [DictionaryCentralizedDKSVD; Alpha*W_1'];
    DictionaryCentralizedDKSVD = normcols(DictionaryCentralizedDKSVD);
    Dic_init_phase2 = DictionaryCentralizedDKSVD;
    %Performing iterations to find a better dictionary
    disp('Starting to  train the discriminative dictionary');
    TrainingDataWithLabels = [TrainingData; labels*Alpha];
    [DictionaryCentralizedDKSVD, output] =  CentralizedKSVD(TrainingDataWithLabels, TotalAtoms, Dic_init_phase2, L , maximumIterations);
    %Renormalizing dic and W
    Dic_c_new = normcols(DictionaryCentralizedDKSVD(1:end-NumberOfClasses,:));
    W = DictionaryCentralizedDKSVD(end-NumberOfClasses+1:end,:)./repmat(sqrt(sum(DictionaryCentralizedDKSVD(1:end-NumberOfClasses,:).^2)),[NumberOfClasses 1]);
    W = W/(Alpha);
    %Classification
    alpha_c = omp(Dic_c_new'*TestData, Dic_c_new'*Dic_c_new, L);
    l = W *alpha_c;
    [maxA, b] = max(l);
    sub = b - double(TestLabels)';
    sub(sub  ~= 0 ) = 1 ;
    for i = 1:NumberOfClasses
        sub2 = TestLabels(TestLabels == i)'- b(TestLabels == i);
        sub2(sub2  ~= 0 ) = 1 ;
        ErrorCentralizedKSVD(CrossValidation,i) = (sum(sub2))/length(find(TestLabels == i));
    end
    save ResultData/GlobalSIP2014/Error_Centralized_KSVD_Imbalance ErrorCentralizedKSVD
    %% Initializations Distributed Setting
    %Initializing Dictionary
    D_init = randn(DataDimensions, TotalAtoms, Nodes);
    for n=1:Nodes
        conv = cell2mat(data_t_distributed(n));
        D_init_1 = conv(:, 1:TotalAtoms);
        D_init(:, :, n) = normcols(D_init_1);
    end
    networkGraph = GenerateGraph(Nodes);
    %% Distributed D-K_SVD
    disp('Training Discriminative Distributed Dictionary')
    %Initializing the dictionary and W
    disp('Dictionary initialization using K-SVD');
    %D_init same as above
    [DistributedDKSVD, ~] = CloudKSVD(data_t_distributed, TotalAtoms, D_init, L, Nodes, networkGraph, maxiterations, 15, 15);
    Dic_d_new = zeros(size(DistributedDKSVD, 1)+NumberOfClasses, size(DistributedDKSVD, 2), size(DistributedDKSVD, 3));
    for n=1:Nodes
        alpha1_c = omp(DistributedDKSVD(:, :, n)'*cell2mat(data_t_distributed(n)), DistributedDKSVD(:, :, n)'*DistributedDKSVD(:, :, n), L);
        W = (alpha1_c*alpha1_c'+eye(TotalAtoms))\(alpha1_c*cell2mat(labels_distributed(n))');
        Dic_d_new(:, :, n) = [DistributedDKSVD(:, :, n); Alpha*W'];
        Dic_d_new(:, :, n) = normcols(Dic_d_new(:, :, n));
    end
    %Performing iterations to find a better dictionary
    disp('Starting to  train the discriminative dictionary');
    DistributedDataWithLabels = cell(Nodes,1);
    for n=1:Nodes
        aa = [cell2mat(data_t_distributed(n));cell2mat(labels_distributed(n))*Alpha];
        DistributedDataWithLabels(n) = {aa};
    end
    [DistributedDKSVD, err] = CloudKSVD(DistributedDataWithLabels, TotalAtoms, Dic_d_new, L, Nodes, networkGraph, maximumIterations, 15, 15);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    W_d=[];
    for n=1:Nodes
        %Renormalizing dic and W
        Dic_d_new = normcols(DistributedDKSVD(1:end-NumberOfClasses,:, n));
        W = DistributedDKSVD(end-NumberOfClasses+1:end,:, n)./repmat(sqrt(sum(DistributedDKSVD(1:end-NumberOfClasses,:, n).^2)),[NumberOfClasses 1]);
        W = W/(Alpha);
        %Classification
        alpha_c = omp(Dic_d_new'*TestData, Dic_d_new'*Dic_d_new, L);
        l = W *alpha_c;
        [maxA, b] =max(l);
        for i = 1:NumberOfClasses
            sub2 = TestLabels(TestLabels == i)'- b(TestLabels == i);
            sub2(sub2  ~= 0 ) = 1 ;
            ErrorDistributedDKSVD(CrossValidation,i,n) = (sum(sub2))/length(find(TestLabels == i));
        end
    end
    save ResultData/GlobalSIP2014/Error_Distributed_DKSVD_Imbalance ErrorDistributedDKSVD
    %% Local D-K_SVD
    W_l=[];
    disp('Training Local Discriminative Dictionaries')
    for n=1:Nodes
        data_tt = cell2mat(data_t_distributed(n));
        %Initializing the dictionary and W
        disp('Dictionary initialization using K-SVD');
        D_init = data_tt(:, 1:TotalAtoms);
        D_init = normcols(D_init);
        [Dic_c, ~] = CentralizedKSVD(data_tt, TotalAtoms, D_init, L , maxiterations);
        alpha1_c = omp(Dic_c'*cell2mat(data_t_distributed(n)), Dic_c'*Dic_c, L);
        W = (alpha1_c*alpha1_c'+eye(TotalAtoms))\(alpha1_c*cell2mat(labels_distributed(n))');
        Dic_c = [Dic_c; Alpha*W'];
        Dic_c_1 = normcols(Dic_c);
        %Performing iterations to find a better dictionary
        disp('Starting to  train the discriminative dictionary');
        data_ttt = [cell2mat(data_t_distributed(n));cell2mat(labels_distributed(n))*Alpha];
        [Dic_c, output] = CentralizedKSVD(data_ttt,TotalAtoms, Dic_c_1, L , maximumIterations);
        %Renormalizing dic and W
        Dic_l_new = normcols(Dic_c(1:end-NumberOfClasses,:));
        W = Dic_c(end-NumberOfClasses+1:end,:)./repmat(sqrt(sum(Dic_c(1:end-NumberOfClasses,:).^2)),[NumberOfClasses 1]);
        W = W/(Alpha);
        %Classification
        alpha_c = omp(Dic_l_new'*TestData, Dic_l_new'*Dic_l_new, L);
        l = W *alpha_c;
        [maxA, b] =max(l);
        for i = 1:NumberOfClasses
            sub2 = TestLabels(TestLabels == i)'- b(TestLabels == i);
            sub2(sub2  ~= 0 ) = 1 ;
            ErrorLocalDKSVD(CrossValidation, i, n) = (sum(sub2))/length(find(TestLabels == i));
        end
    end
    save ResultData/GlobalSIP2014/Error_Local_DKSVD_Imbalance ErrorLocalDKSVD
end